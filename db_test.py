import psycopg2
from config import config

def connect():

    conn = None
    try:
        params = config()
        #Connect to postgresql server
        print('Connecting to Database Server...')
        conn = psycopg2.connect(**params)

        #Create a cursor
        cur = conn.cursor()

        #Execute commands including SQL
        print('PostgreSQL Version:')
        cur.execute('SELECT version()')

        #Display te version using fetchone
        db_version = cur.fetchone()
        print(db_version)

        #close the connection of postgresql
        cur.close()

    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print('Database Connection Closed.')

if __name__ == '__main__':
    connect()


        